//
//  DetailViewController.m
//  Segue
//
//  Created by Anna Pasternak on 09/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import "DetailViewController.h"
#import "NamesRepository.h"

@implementation DetailViewController




- (void)viewDidLoad {
    [super viewDidLoad];
    self.FirstName.text = self.DetailsModel.firstName;
    self.Date.text = self.DetailsModel.birth;
    self.Description.text = self.DetailsModel.nameDescription;
    self.Short.text = self.DetailsModel.shortFirstName;
    self.Number.text = self.DetailsModel.number;
    self.Color.text = self.DetailsModel.color;
    self.Stone.text = self.DetailsModel.stone;
    self.Plant.text = self.DetailsModel.plant;
    self.Animal.text = self.DetailsModel.animal;
    self.Planet.text = self.DetailsModel.planet;
    self.Sayings.text = self.DetailsModel.sayings;
    self.Song.text = self.DetailsModel.songs;

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
