//
//  TableCell.h
//  Segue
//
//  Created by Anna Pasternak on 09/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *TitleLabel;
@property (strong, nonatomic) IBOutlet UILabel *InfoLabel;


@end
