//
//  main.m
//  Segue
//
//  Created by Anna Pasternak on 09/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
