//
//  NamesRepository.m
//  Segue
//
//  Created by Anna Pasternak on 11/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "NamesRepository.h"

@implementation NamesRepository

static NamesRepository *_repository;
+ (NamesRepository*) getRepository{
    if (_repository == nil) {
        _repository = [[NamesRepository alloc]init];
    }
    return _repository;
}

- (id) init{
    if(self = [super init])
    {
        NSString *sqlitePath = [[NSBundle mainBundle] pathForResource:@"znaczenie2" ofType:@"db"];
        if(sqlite3_open([sqlitePath UTF8String], &_database) != SQLITE_OK){
            NSLog(@"Failed to open database");
        }
        
    }
    return self;
}
- (NSArray*) getNames{
    NSMutableArray *data = [[NSMutableArray alloc]init];
    NSString *query = @"SELECT firstName FROM Znaczenie";
    sqlite3_stmt *statement;
    if(sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
        while (sqlite3_step(statement) == SQLITE_ROW) {
            char *n = (char *)sqlite3_column_text(statement, 0);
            
            NSString *name = [[NSString alloc] initWithUTF8String:n];
            
            // NameObject *row = [[NameObject alloc]initWithData:name];
            
            [data addObject:name];
            
            
        }
    }
    return data;
}

-(NameDetails *)getDetails:(NSString *)firstName
{
    //or split line in c style "\"
    NameDetails *result = nil;
    NSString *query = @"SELECT "
        "birth, "
        "description, "
        "shortFirstName, "
        "number, "
        "color, "
        "kamien, "
        "roslina, "
        "zwierze, "
        "planeta, "
        "przyslowia, "
        "piosenki "
        "FROM Znaczenie WHERE firstName=?";
    sqlite3_stmt *statement;
    if(sqlite3_prepare_v2(_database, [query UTF8String], -1, &statement, nil) == SQLITE_OK)
    {
      
        sqlite3_bind_text(statement, 1, [firstName UTF8String], -1, SQLITE_TRANSIENT);
        while (sqlite3_step(statement) == SQLITE_ROW) {
            
            
            result = [[NameDetails alloc]initWithName:firstName
                                                birth:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)]
                                          description:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 1)]
                                       shortFirstName:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 2)]
                                               number:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 3)]
                                                color:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 4)]
                                                stone:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 5)]
                                                plant:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 6)]
                                               animal:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 7)]
                                               planet:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 8)]
                                              sayings:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 9)]
                                                songs:[NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 10)]];
           // NSString *name = [NSString stringWithUTF8String:(char *)sqlite3_column_text(statement, 0)];
            
            // NameObject *row = [[NameObject alloc]initWithData:name];
        }
    }
    return result;
    
}

- (void)dealloc{
    sqlite3_close(_database);
}
-(IBAction)myTableView:(id)sender;
{
    
}

@end
