//
//  NamesRepository.h
//  Segue
//
//  Created by Anna Pasternak on 11/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DetailViewController.h"
#import <sqlite3.h>
@interface NamesRepository : NSObject{
    sqlite3 *_database;
}

+ (NamesRepository *) getRepository;
- (NSArray *) getNames;
- (DetailViewController *) getDetails:(NSString *) firstName;



@end