//
//  NameDetails.m
//  Segue
//
//  Created by Marcin Pasternak on 13/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import "NameDetails.h"

@implementation NameDetails

-(NameDetails *) initWithName:(NSString *)firstName birth:(NSString *)birth description:(NSString *)nameDescription shortFirstName:(NSString *)shortFirstName number:(NSString *)number color:(NSString *)color stone:(NSString *)stone plant:(NSString *)plant animal:(NSString *)animal planet:(NSString *)planet sayings:(NSString *)sayings songs:(NSString *)songs;

{
    if(self = [super init])
    {
        self.firstName = firstName;
        self.birth = birth;
        self.nameDescription = nameDescription;
        self.shortFirstName = shortFirstName;
        self.number = number;
        self.color = color;
        self.stone = stone;
        self.plant = plant;
        self.animal = animal;
        self.planet = planet;
        self.sayings = sayings;
        self.songs = songs;
    }
    return self;
    
    
}
@end
