//
//  DetailViewController.h
//  Segue
//
//  Created by Anna Pasternak on 09/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "NameDetails.h"

@interface DetailViewController : UIViewController  {
    
}
@property (weak, nonatomic) IBOutlet UILabel *FirstName;
@property (weak, nonatomic) IBOutlet UILabel *Date;
@property (weak, nonatomic) IBOutlet UILabel *Description;
@property (weak, nonatomic) IBOutlet UILabel *Short;
@property (weak, nonatomic) IBOutlet UILabel *Number;
@property (weak, nonatomic) IBOutlet UILabel *Color;
@property (weak, nonatomic) IBOutlet UILabel *Stone;
@property (weak, nonatomic) IBOutlet UILabel *Plant;
@property (weak, nonatomic) IBOutlet UILabel *Animal;
@property (weak, nonatomic) IBOutlet UILabel *Planet;
@property (weak, nonatomic) IBOutlet UILabel *Sayings;
@property (weak, nonatomic) IBOutlet UILabel *Song;

@property (strong, nonatomic) NameDetails *DetailsModel;

@end
