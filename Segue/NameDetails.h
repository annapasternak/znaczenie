//
//  NameDetails.h
//  Segue
//
//  Created by Marcin Pasternak on 13/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NameDetails : NSObject

@property (strong, nonatomic) NSString *firstName;
@property (strong, nonatomic) NSString *birth;
@property (strong, nonatomic) NSString *nameDescription;
@property (strong, nonatomic) NSString *shortFirstName;
@property (strong, nonatomic) NSString *number;
@property (strong, nonatomic) NSString *color;
@property (strong, nonatomic) NSString *stone;
@property (strong, nonatomic) NSString *plant;
@property (strong, nonatomic) NSString *animal;
@property (strong, nonatomic) NSString *planet;
@property (strong, nonatomic) NSString *sayings;
@property (strong, nonatomic) NSString *songs;

-(NameDetails *) initWithName:(NSString *)firstName birth:(NSString *)birth description:(NSString *)nameDescription shortFirstName:(NSString *)shortFirstName number:(NSString *)number color:(NSString *)color stone:(NSString *)stone plant:(NSString *)plant animal:(NSString *)animal planet:(NSString *)planet sayings:(NSString *)sayings songs:(NSString *)songs;

@end
