//
//  TableViewController.h
//  Segue
//
//  Created by Anna Pasternak on 09/09/15.
//  Copyright (c) 2015 Anna Pasternak. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewController : UITableViewController<UISearchBarDelegate>


@property (nonatomic, strong) NSArray *Title;
@property (nonatomic, strong) NSArray *Info;
@property (weak, nonatomic) IBOutlet UISearchBar *mySearchBar;


@property (weak, nonatomic) IBOutlet UITableView *myTableView;

@end
